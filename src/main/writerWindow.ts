import { BrowserWindow } from 'electron';

const UI_BASE_URL = require('./uiBaseUrl');

export class WriterWindow {
  
  browserWindow: BrowserWindow;

  constructor() {
    this.browserWindow = new BrowserWindow({
      alwaysOnTop: true,
      frame: false,
      transparent: true,
      vibrancy: 'dark',
      width: 1,
      height: 1
    });

    this.browserWindow.loadURL(`${UI_BASE_URL}writer.html`);
  }

  setText(text: String) {
    this.browserWindow.webContents.send('set-text', text);
  }
};