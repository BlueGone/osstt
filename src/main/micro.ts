import * as Mic from 'mic';

export function createMicrophoneInstance() {
  return Mic({
    rate: '16000',
    channels: '1',
    debug: false,
    exitOnSilence: 20,
  })
}
