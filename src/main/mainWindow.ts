import { BrowserWindow } from 'electron';

const UI_BASE_URL = require('./uiBaseUrl');

export class MainWindow {
  browserWindow: BrowserWindow;

  constructor() {
    this.browserWindow = new BrowserWindow({
      width: 800,
      height: 480,
      titleBarStyle: 'hidden',
      transparent: true,
      vibrancy: 'dark',
    });

    this.browserWindow.loadURL(`${UI_BASE_URL}main.html`);
  }
};
