import * as speech from '@google-cloud/speech';

const client = new speech.SpeechClient();

export function createStreamingRecognizer(language: string) {
  return client.streamingRecognize({
    config: {
      encoding: 'LINEAR16',
      sampleRateHertz: 16000,
      languageCode: 'fr-FR',
    },
    interimResults: true,
  })
};
