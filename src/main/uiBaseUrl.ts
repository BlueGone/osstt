const R = require('ramda');

module.exports = R.defaultTo('http://localhost:8080/', process.env.UI_BASE_URL);
