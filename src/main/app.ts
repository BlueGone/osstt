import { app, ipcMain, globalShortcut } from 'electron';
import { MainWindow } from './mainWindow';
import { WriterWindow } from './writerWindow';
import { createStreamingRecognizer } from './speech';
import { createMicrophoneInstance } from './micro';
import rec from '../helpers/rec';
import settings from './settings';

app.on('ready', () => {
  let writerWindow: WriterWindow = null;
  let mainWindow: MainWindow = new MainWindow();

  ipcMain.on('start-speech', () => {

    mainWindow.browserWindow.minimize();
    writerWindow = new WriterWindow();

    let microphoneInstance = createMicrophoneInstance();
    let microphoneInstanceAudioStream = microphoneInstance.getAudioStream();

    let recognizeStream = createStreamingRecognizer(settings.language);
    microphoneInstanceAudioStream.pipe(recognizeStream);

    globalShortcut.register('CommandOrControl+Shift+B', () => {
      writerWindow.browserWindow.minimize();
      writerWindow.browserWindow.close();
      writerWindow = null;
      recognizeStream.end();
      microphoneInstanceAudioStream.unpipe(recognizeStream);
      microphoneInstance.stop();
      mainWindow.browserWindow.show();
      globalShortcut.unregister('CommandOrControl+Shift+B');
    });

    recognizeStream.on(
      'data',
      rec((cb, data: any) => {
        if (data.results[0].isFinal || !(data.results[0] && data.results[0].alternatives[0])) {
          microphoneInstance.pause();
          recognizeStream.end();
          microphoneInstanceAudioStream.unpipe(recognizeStream);
          recognizeStream = createStreamingRecognizer(settings.language);
          recognizeStream.on('data', cb);
          microphoneInstanceAudioStream.pipe(recognizeStream);
          microphoneInstance.resume();
        }
        if (writerWindow != null) {
          writerWindow.setText(data.results[0].alternatives[0].transcript);
        }
      })
    );

    microphoneInstance.start();
  });
});
