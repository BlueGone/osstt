export const rec = (f: (...args: any[]) => any) => (...args: any) => f(rec(f), ...args);
export default rec;
