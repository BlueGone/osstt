import Vue from 'vue';
import Writer from './components/Writer.vue';

new Vue({
  render: h => h(Writer),
}).$mount('#writer');
