const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const path = require('path');

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'src/renderer/main/app.js'),
    writer: path.resolve(__dirname, 'src/renderer/writer/app.js'),
  },
  output: {
    path: path.resolve(__dirname, 'dist/ui/'),
  },
  target: 'electron-renderer',
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.s[ca]ss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: [
                path.resolve(__dirname, 'node_modules', 'bulma'),
              ],
            },
          },
        ],
      },
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/renderer/main/index.html'),
      filename: 'main.html',
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/renderer/writer/index.html'),
      filename: 'writer.html',
    }),
    new VueLoaderPlugin(),
  ],
  devServer: {
    publicPath: '/',
  },
};
